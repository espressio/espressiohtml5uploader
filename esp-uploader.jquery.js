"use strict";

/*!
 * Espressio HTML5 & jQuery multipart file uploader
 * @copyright Copyright (c) 2012-2016 Espressio Labs Ltd. (https://bitbucket.org/espressio)
 * @author    Sergey Basov (sergey@espressio.ru)
 */
(function ($) {

	/**
	 * Methods object
	 * @type Object
	 */
	var methods = {};

	/**
	 * HTML5 multipart file uploader.
	 * May be applied to input type file or element
	 * with the same input inside.
	 *
	 * @param   {Object} [options]
	 * @returns jQuery
	 */
	methods.init = function (options) {

		if (!options.uploadUrl) {
			options.uploadUrl = location.href;
			window.console && console.warn(
				'Upload URL is not set in EspressioHtml5Uploader options. Current URL is used.'
			);
		}

		/**
		 * Default options values
		 */
		options = $.extend(
			{
				// File portion size in bytes
				portion: 1048576,

				// Response timeout
				timeout: 15000,

				// Use onChange handlers, set to input
				// instead of specified in plugin
				useDocumentChangeEvent: false,

				// Success result keyword sent to server
				resultSuccess: 'success',

				// Error result keyword sent to server
				resultError: 'error'
			},
			options
		);

		/**
		 * File input change handler
		 *
		 * @returns void
		 */
		var inputOnChange = function (e) {
			var file = e.target.files;
			file = file[0];
			if (file) {
				$(this).trigger('uploadStart');

				file.uploadId = generateRndId();

				if (file.size > options.portion) {
					uploadFilePortion(file, 0, options.portion, $(this));
				} else {
					uploadFilePortion(file, 0, file.size, $(this));
				}
			}
		};

		/**
		 * One input[type="file"] initialization
		 *
		 * @returns void
		 */
		var make = function () {

			if (!browserSupport()) {
				return;
			}

			if (!options.uploadUrl) {
				return;
			}

			if ($(this).is('input[type="file"]')) {
				var fileElements = $(this);
			} else {
				var fileElements = $(this).find('input[type="file"]');
			}

			if (!fileElements || !fileElements.length) {
				window.console && console.warn('No file input found');
				return;
			}

			fileElements.data('uploadDenied', '0');

			if (!options.useDocumentChangeEvent) {
				fileElements.change(inputOnChange);
			} else {
				fileElements.each(function () {
					if ($(this).attr('id')) {
						var inputId = $(this).attr('id');
					} else {
						var inputId = 'aii' + generateRndId();
						$(this).attr('id', inputId);
					}
					$(document).on('change', '#' + inputId, fileElements);
				});
			}
		};

		/**
		 * Upload file portion
		 *
		 * @param file   {Object}
		 * @param offset {number}
		 * @param size   {number}
		 * @param obj    {Object}
		 * @returns {Boolean}
		 */
		var uploadFilePortion = function (file, offset, size, obj) {

			if (!file) {
				return false;
			}

			if (!offset) {
				offset = 0;
			}

			if (!offset) {
				obj.data('uploadDenied', '0');
			}

			if (obj && parseInt(obj.data('uploadDenied')) == 1) {
				obj.data('uploadDenied', '0');
				obj.trigger('uploadEnd', {file: file});
				obj.trigger('uploadCancel', {file: file});
				obj.val('');

				jQuery.ajax(
					options.uploadUrl,
					{
						type: 'POST',
						data: {
							upload_result: options.resultError
						},
						headers: {
							'Upload-Id': file.uploadId
						}
					}
				);

				return false;
			}

			if (!size) {
				size = options.portion;
			}

			var blob = null;
			var method = (file.slice ?
				'slice' :
				(file.webkitSlice ? 'webkitSlice' : (file.mozSlice ? 'mozSlice' : false))
			);

			if (method) {
				blob = file[method](offset, offset + size);
			}

			var xhr = new XMLHttpRequest();

			xhr.open('POST', options.uploadUrl, true);
			xhr.setRequestHeader('Content-Type', 'application/octet-stream; charset=utf-8');
			xhr.setRequestHeader('Upload-Id', file.uploadId);
			xhr.setRequestHeader('Portion-From', offset);
			xhr.setRequestHeader('Portion-Size', size);
			xhr.setRequestHeader(
				'Content-Disposition',
				'attachment, filename="' + encodeURI(file.name) + '"'
			);

			xhr.responseType = 'blob';
			xhr.withCredentials = true;
			xhr.timeout = options.timeout;

			xhr.ontimeout = function () {
				window.console && console.error('Portion upload failed: timeout error');
				if (obj) {
					obj.trigger('uploadPortionFail', {file: file, error: 'timeout'});
				}
				uploadFilePortion(file, offset, size, obj);
			};

			xhr.upload.addEventListener('progress', function (e) {
				if (obj) {
					var data = e;
					data.file = file;
					if (e.lengthComputable && e.total) {
						data.complete = e.loaded / e.total;
					}
					obj.trigger('uploadProgress', data);
				}
			});

			xhr.addEventListener('load', function (e) {

				if (e.target.status != 200) {
					if (window.console) {
						console.error('HTML5 Upload: failed (code: ' + e.target.status + ')');
					}
					return;
				}

				if (obj) {
					var data = {
						file: file,
						offset: offset,
						size: size,
						total: file.size,
						uploaded: offset + size,
						complete: 0
					};
					if (data.total) {
						data.complete = data.uploaded / data.total * 100;
						if (data.complete > 100) {
							data.complete = 100;
						}
					}
					obj.trigger('uploadPortionComplete', data);
				}

				if (file.size > offset + size) {
					uploadFilePortion(file, offset + size, options.portion, obj);
				} else {
					jQuery.ajax(
						options.uploadUrl,
						{
							type: 'POST',
							data: {
								upload_result: options.resultSuccess,
								filename: encodeURI(file.name)
							},
							headers: {
								'Upload-Id': file.uploadId
							}
						}
					).done(function (data) {
						if (obj) {
							obj.trigger('uploadEnd', {file: file});
							obj.trigger('uploadComplete', {file: file, answer: data});
						}
					});
				}
			});

			xhr.addEventListener('error', function () {

				jQuery.ajax(
					options.uploadUrl,
					{
						type: 'POST',
						data: {
							upload_result: options.resultError,
							filename: encodeURI(file.name)
						},
						headers: {
							'Upload-Id': file.uploadId
						}
					}
				);

				if (obj) {
					obj.trigger('uploadEnd', {file: file});
					obj.trigger('uploadFail', {xhr: rxhr, status: status, error: error});
				}
			});

			xhr.send(blob);
		};

		/**
		 * Check if browser is compatible with html5 upload
		 * @returns {Boolean}
		 */
		var browserSupport = function () {
			var res = (window.File && window.FileReader && window.FileList && window.Blob);
			if (!res && window.console) {
				console.error('HTML5 upload is not supported by browser');
			}

			return res;
		};

		/**
		 * Generate random string
		 * @returns {string}
		 */
		var generateRndId = function () {
			return new Date().getTime() + '' + Math.floor(Math.random() * 10E9)
		};

		if (!browserSupport()) {
			this.trigger('uploadNotSupported');
		}

		return this.each(make);
	};

	/**
	 * Cancel elements uploads
	 * @returns jQuery
	 */
	methods.cancel = function () {
		return this.each(function () {
			$(this).val('');
			$(this).data('uploadDenied', '1');
		});
	};

	/**
	 * Reset elements values
	 * @returns jQuery
	 */
	methods.reset = function () {
		return this.each(function () {
			$(this).val('');
			$(this).trigger('uploadReset');
			$(this).html5upload('cancel');
		});
	};

	/**
	 * Plugin initialization
	 *
	 * @param {string|Object} method
	 * @returns jQuery
	 */
	jQuery.fn.espHtml5Uploader = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Unknown method ' + method + ' in jQuery.espHtml5Uploader');
		}
	};

})(jQuery);