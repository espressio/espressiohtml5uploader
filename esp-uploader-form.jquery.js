"use strict";

/*!
 * Espressio uploader form
 * @copyright Copyright (c) 2012-2016 Espressio Labs Ltd. (https://bitbucket.org/espressio)
 * @author    Sergey Basov (sergey@espressio.ru)
 */

(function ($) {

	/**
	 * HTML5 form processing for html5upload plugin
	 *
	 * @requires jQuery.espHtml5Uploader
	 * @param   {string} [uploadUrl]
	 * @param   {Object} [options]
	 * @returns jQuery
	 */
	jQuery.fn.espHtml5UploaderForm = function (uploadUrl, options) {

		options = $.extend(
			{
				// File portion size in bytes
				portion: 1048576,

				// Response timeout
				timeout: 15000,

				// Use onChange handlers, set to input
				// instead of specified in plugin
				useDocumentChangeEvent: false,

				// Elements classes prefix
				classesPrefix: 'esp-upl-',

				// Generate progress bar element
				addProgressBar: true,

				// Add upload progress in percents element
				addPercent: true,

				// Add cancel button element
				addCancel: true
			},
			options
		);

		/**
		 * Get class name with prefix
		 * @param {string} className
		 * @param {boolean} [asSelector]
		 * @returns {string}
		 */
		var _c = function(className, asSelector) {
			var res = '' + options.classesPrefix + className;
			return (asSelector ? '.' : '') + res;
		};

		/**
		 * One form initialization
		 * @returns void
		 */
		var make = function () {

			var fileElements = $(this).find('input[type="file"]');
			var uploadedCount = 0;
			var filesToUpload = 0;

			if (!fileElements || !fileElements.length) {
				window.console && console.warn('No upload inputs found');
				return;
			}

			fileElements.each(function () {
				if (options.addProgressBar || options.addPercent || options.addCancel) {
					if (!$(this).next().is(_c('progress-box', true))) {
						$(this).after(
							'<div class="' + _c('progress-box') + '"></div>'
						);
					}
					var box = $(this).next();

					if (options.addProgressBar) {
						box.prepend('<div class="' + _c('progress') + '"></div>');
					}

					if (options.addPercent) {
						box.append('<div class="' + _c('percent') + '"></div>');
					}

					if (options.addCancel) {
						var currentInput = $(this);
						var cancelButton = $('<div class="' + _c('cancel') + '"></div>');
						box.append(cancelButton);
						cancelButton.click(function () {
							currentInput.espHtml5Uploader('cancel');
						}).hide();
					}
				}

				$(this).trigger('uploadElementReady');
			});

			/**
			 * EspressioHtml5Uploader options
			 * @type Object
			 */
			var uploaderOptions = {
				uploadUrl: (uploadUrl ? uploadUrl : $(this).attr('action')),
				portion: options.portion,
				timeout: options.timeout,
				useDocumentChangeEvent: options.useDocumentChangeEvent
			};

			fileElements.espHtml5Uploader('init', uploaderOptions);

			fileElements.on('uploadPortionComplete', function (e, data) {
				if (options.addProgressBar) {
					$(this).next().find(_c('progress', true)).css('width', data.complete + '%');
				}
				if (options.addPercent) {
					$(this).next().find(_c('percent', true)).html(Math.round(data.complete) + '%');
				}
			})

			.on('uploadFail', function () {
				filesToUpload--;
				$(this).next().find(_c('progress-box', true)).addClass(_c('fail'));
			})

			.on('uploadStart', function () {
				if (options.addProgressBar) {
					$(this).next().find(_c('progress', true)).css('width', 0);
				}
				if (options.addPercent) {
					$(this).next().find(_c('percent', true)).html('');
				}
				filesToUpload++;
				$(this).attr('disabled', 'disabled');
				if (options.addCancel) {
					$(this).next().find(_c('cancel', true)).show();
				}
			})

			.on('uploadEnd', function () {
				uploadedCount++;
				$(this).removeAttr('disabled');
				if (options.addCancel) {
					$(this).next().find(_c('cancel', true)).hide();
				}
			})

			.on('uploadCancel', function () {
				filesToUpload--;
				if (options.addProgressBar) {
					$(this).next().find(_c('progress', true)).css('width', 0);
				}
				if (options.addPercent) {
					$(this).next().find(_c('percent', true)).html('');
				}
			})

			.on('uploadComplete', {form: $(this)}, function (e) {
				if (uploadedCount >= filesToUpload) {
					e.data.form.trigger('uploadsComplete');
				}
			})

			.on('uploadNotSupported', {form: $(this)}, function (e) {
				e.data.form.addClass(_c('not-supported'));
			})

			.on('uploadReset', function () {

			});

			$(this).submit(function (e) {
				if (uploadedCount < filesToUpload) {
					e.preventDefault();
				}
			});
		};

		return this.each(make);
	};

})(jQuery);