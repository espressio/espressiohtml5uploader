<?php

/**
 * Espressio HTML5 uploader plugin requests handler
 *
 * @copyright Copyright (c) 2012-2016 Espressio Labs Ltd. (https://bitbucket.org/espressio)
 * @author    Sergey Basov (sergey@espressio.ru)
 */
class EspressioUploaderHandler
{
	/**
	 * Error result keyword
	 * @const string
	 */
	const RESULT_ERROR = 'error';

	/**
	 * Success result keyword
	 * @const string
	 */
	const RESULT_SUCCESS = 'success';

	/**
	 * Temporary file name postfix
	 * @const string
	 */
	const TMP_EXT = '.esp-uploader.tmp';

	/**
	 * Upload directory path
	 * @var string
	 */
	protected $_path = null;

	/**
	 * EspressioUploaderHandler constructor.
	 *
	 * @param string $uploadDir
	 * @throws Exception
	 */
	public function __construct($uploadDir)
	{
		if (empty($uploadDir) || !is_string($uploadDir)) {
			throw new Exception('Empty or invalid upload directory path given: ' . gettype($uploadDir));
		}
		if (!is_dir($uploadDir) || !is_writable($uploadDir)) {
			throw new Exception('Specified upload dir is not writable: ' . $uploadDir);
		}

		$this->_path = realpath($uploadDir) . DIRECTORY_SEPARATOR;
	}

	/**
	 * Handle uploader request.
	 * Reads data from request headers, sent by EspHtml5Uploader plugin.
	 * Returns:
	 *      false, if part upload is failed
	 *      true, if part upload is succeed
	 *      string — uploaded file path, if upload is finished
	 *
	 * @return bool|string
	 * @throws Exception
	 */
	public function upload()
	{
		if (empty($_SERVER['HTTP_UPLOAD_ID']) || !is_numeric($_SERVER['HTTP_UPLOAD_ID'])) {
			throw new Exception('HTTP_UPLOAD_ID is not a valid numeric value');
		}
		if ($_SERVER['REQUEST_METHOD'] != 'POST') {
			throw new Exception('Incorrect request method');
		}

		$fileId = $_SERVER['HTTP_UPLOAD_ID'];
		$path   = $this->_path;

		/**
		 * Handle upload end request
		 */
		if (!empty($_POST['upload_result'])) {

			/**
			 * Upload failed
			 */
			if ($_POST['upload_result'] == static::RESULT_ERROR) {
				if (file_exists($path . $fileId . static::TMP_EXT)) {
					unlink($path . $fileId . static::TMP_EXT);
				}

				return false;
			}

			/**
			 * Upload successfully ends
			 */
			if ($_POST['upload_result'] == static::RESULT_SUCCESS) {
				if (file_exists($path . $fileId . static::TMP_EXT)) {
					if (!empty($_POST['filename'])) {
						$filename = urldecode($_POST['filename']);
						$filename = str_replace(
							array('/', '\\', '..'),
							'',
							$filename
						);
					}
					if (empty($filename)) {
						$filename = $fileId;
					}
					rename($path . $fileId . static::TMP_EXT, $path . $filename);

					return $path . $filename;
				}

				return false;
			}
		}

		/**
		 * Upload portion
		 */

		$filename = $path . $fileId . static::TMP_EXT;
		$offset   = (int)$_SERVER['HTTP_PORTION_FROM'];

		if (!$offset) {
			$fout = fopen($filename, 'wb');
		} else {
			$fout = fopen($filename, 'ab');
		}

		if (!$fout) {
			throw new Exception('Cannot open file ' . $filename . ' for writing');
		}

		$data = file_get_contents('php://input');

		fwrite($fout, $data);

		fclose($fout);

		return true;
	}
}